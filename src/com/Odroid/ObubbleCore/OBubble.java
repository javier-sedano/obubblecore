package com.Odroid.ObubbleCore;

import android.R.drawable;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class OBubble extends Activity {
  private LevelView _levelView;

  private static final int MENU_ABOUT = 1; 
  private static final int MENU_WEB = 2; 
  static final int MENU_SETTINGS = 3;
  private static final int MENU_MARKET = 4; 
  
  PowerManager _powerManagement = null;
  PowerManager.WakeLock _wakeLock = null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.level);

    _levelView = (LevelView) findViewById(R.id.level);
    _levelView.setTextViews(
        (TextView) findViewById(R.id.text0),
        (TextView) findViewById(R.id.text1),
        (TextView) findViewById(R.id.text2));

    Preferences.setSharedPreferences(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()));
    
    if (savedInstanceState == null) {
      _levelView.initState();
      Log.i(this.getClass().getName(), "SIS is null");
    } else {
      _levelView.restoreState(savedInstanceState);
      Log.i(this.getClass().getName(), "SIS is nonnull");
    }
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    _levelView.saveState(outState);
    Log.i(this.getClass().getName(), "SIS called");
  }

  @Override
  protected void onResume(){
    super.onResume();
    _keepOnStart();
  }

  @Override
  protected void onPause() {
    _levelView.onPause();
    _keepOnStop();
    super.onPause();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    super.onCreateOptionsMenu(menu);
    menu.add(0, MENU_SETTINGS, 0, R.string.menu_settings).setIcon(drawable.ic_menu_preferences);
    menu.add(0, MENU_ABOUT, 0, R.string.menu_about).setIcon(drawable.ic_menu_info_details);
    menu.add(0, MENU_WEB, 0, R.string.menu_web).setIcon(drawable.ic_menu_add);
    menu.add(0, MENU_MARKET, 0, R.string.menu_market).setIcon(drawable.ic_menu_add);
    return true;
  }
  
  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
    case MENU_SETTINGS:
      Intent preferencesIntent = new Intent(getApplicationContext(), Preferences.class);
      startActivity(preferencesIntent);
      return true;
    case MENU_ABOUT:
      AlertDialog.Builder builder = new AlertDialog.Builder(this);
      builder.setTitle(R.string.about_title)
             .setMessage(R.string.about_text)
             .setCancelable(false)
             .setIcon(R.drawable.icon)
             .setNeutralButton(R.string.about_button, new DialogInterface.OnClickListener() {
                 public void onClick(DialogInterface dialog, int id) {
                      dialog.cancel();
                 }
             })
             .show();
      break;
    case MENU_WEB:
      _web();
      break;
    case MENU_MARKET:
      _market();
      break;
    }
    return false;
  }
  
  private void _web() {
    String url = getString(R.string.web);
    Intent i = new Intent(Intent.ACTION_VIEW);
    i.setData(Uri.parse(url));
    startActivity(i);
  }
  
  private void _market() {
    String url = getString(R.string.market);
    Intent i = new Intent(Intent.ACTION_VIEW);
    i.setData(Uri.parse(url));
    try {
      startActivity(i);
    } catch (ActivityNotFoundException e) {
      _web();
    }
  }
  
  private void _keepOnStart() {
    if (_powerManagement == null) {
      _powerManagement = (PowerManager) getSystemService(Context.POWER_SERVICE);
    }
    if (_wakeLock == null) {
      _wakeLock = _powerManagement.newWakeLock(
          PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ON_AFTER_RELEASE,
          "0 Bubble keep on");
    }
    _wakeLock.acquire();
  }
  
  private void _keepOnStop() {
    if ((_wakeLock != null) && (_wakeLock.isHeld())) {
      _wakeLock.release();
    }
  }

  
}
