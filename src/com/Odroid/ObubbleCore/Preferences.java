package com.Odroid.ObubbleCore;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;

public class Preferences extends PreferenceActivity {
  private static SharedPreferences _prefs;
  
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Load the preferences from an XML
    addPreferencesFromResource(R.layout.preferences);
  }

  public static void setSharedPreferences(SharedPreferences s) {
    _prefs = s;
  }
  
  public static boolean getVibrate() {
    if (_prefs == null) return false;
    return _prefs.getBoolean("vibrate", false);
  }

  public static boolean getUseGallery() {
    if (_prefs == null) return false;
    return _prefs.getBoolean("useGallery", false);
  }
  public static void setUseGallery(boolean useGallery) {
    if (_prefs == null) return ;
    SharedPreferences.Editor e = _prefs.edit();
    e.putBoolean("useGallery", useGallery);
    e.commit();
  }

  public static float getFxVolume() {
    if (_prefs == null) return 1.0f;
    String vol = _prefs.getString("fxVolume", "1.0");
    Float f = new Float(vol);
    return f.floatValue();
  }

}