package com.Odroid.ObubbleCore;

import java.io.FileNotFoundException;

import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.TextView;


public class LevelView extends SurfaceView implements SurfaceHolder.Callback, View.OnClickListener, View.OnTouchListener {

  private Context _context;
  private TextView _text0Text;
  private TextView _text1Text;
  private TextView _text2Text;
  SurfaceHolder _surfaceHolder = null;

  private Bitmap _bgBitmap;
  private Bitmap _lifeBitmap;
  private static final int LIFE_BITMAP_WIDTH = 16;
  private static final int LIFE_BITMAP_HEIGTH = 16;
  private static final int LEVEL_FONT_HEIGTH = 60;
  private static final int BALL_RAD = 5;
  private int _canvasHeight = 1;
  private int _canvasWidth = 1;
  private Paint _orangePaint = null;
  private Bitmap _ballBitmap;
  private Bitmap _bubbleBitmap;
  private Handler timer = new Handler();
  private Runnable onTimerRunnable = new Runnable() {
    public void run() {
      onTimer();
    }
  };
  MediaPlayer _applauseMedia;
  MediaPlayer _bangMedia;
  private static final int MAX_BUILTIN_IMAGES = 5;
  private static final int MAX_BUBBLES = 30;
  private int MAX_BOUNCE_SOUND = 1;
  
  protected static final int STATE_GREET = 0;
  protected static final int STATE_LOSE = 1;
  protected static final int STATE_WIN = 2;
  protected static final int STATE_RUNNING = 3;
  protected static final int STATE_PAUSE = 4;
  protected static final int STATE_LOSTLIFE = 5;

  private static final String SIS_BALLS = "BALLS";
  private static final String SIS_BUBBLES = "BUBBLES";
  private static final String SIS_LIVES = "LIVES";
  private static final String SIS_LEVEL = "LEVEL";
  private static final String SIS_BGNUMBER = "BGNUMBER";
  private static final String SIS_STATE = "STATE";
  
  private static final int[] TEXT0 = new int[7]; // One more than the number of states
  private static final int[] TEXT1 = new int[7]; // One more than the number of states
  private static final int[] TEXT2 = new int[9]; // One more than the number of hints
  
  // Physics
  private static final long MS_PER_FRAME = 40; // 25fps is 40ms/f
  private float _ball_speed_max = 1; // px/ms
  private static final int BALL_SPEED_SCALE = 10000; // ms to cross the diagonal (max)
  private float _bubble_deceleration = 1; // px/ms^2
  private static final float BUBBLE_DECELERATION_SCALE = 100000; // ms to decelerate from |diagonal| px/ms to 0 for radius=1px
  private float _bubble_gravity = 1; // px/ms^2
  private static final float BUBBLE_GRAVITY_SCALE = 30000; // ms to acelerate from 0 to |diagonal| px/ms
  private float _bubble_growth = 1; // px/ms
  private static final float BUBBLE_GROWTH_SCALE = 10000; // ms to grow from 0 to diagonal
  private float _fx_thereshold = 0.0001f;
  private static final float FX_THERESHOLD_SCALE = 5*1000*1000;
  private static final float AREA_TARGET = 0.4f;
  
  // Unsaved game state
  private long _lastTime = 0;
  private long _lastSleep = 0;
  private int _inflating_center_x = -1;
  private int _inflating_center_y = -1;
  private int _inflating_x = -1;
  private int _inflating_y = -1;
  private int _inflating_r = -1; // <0 to mean "not inflating"
  private int _bouncesounds = 0;

  // Game state
  protected int _state = STATE_PAUSE;
  private int _lives = 0;
  private int _level = 0;
  Balls _balls = null;
  Bubbles _bubbles = null;
  int _bgNumber = 0;

  public LevelView(Context context, AttributeSet attrs) {
    super(context, attrs);
    _context = context;
    _surfaceHolder = getHolder();
    _surfaceHolder.addCallback(this);
    _lifeBitmap = BitmapFactory.decodeResource(_context.getResources(), R.drawable.life);
    _setSpecificBackground(0); // Just in case
    _orangePaint = new Paint();
    _orangePaint.setColor(0xc0d69100);
    _orangePaint.setTextSize(LEVEL_FONT_HEIGTH*1f);
    _ballBitmap = BitmapFactory.decodeResource(_context.getResources(), R.drawable.ball);
    _bubbleBitmap = BitmapFactory.decodeResource(_context.getResources(), R.drawable.bubble);
    _applauseMedia = MediaPlayer.create(_context, R.raw.aplause);
    _bangMedia = MediaPlayer.create(_context, R.raw.bang);
    _fillTexts();
    setOnClickListener(this);
    setOnTouchListener(this);
  }

  private void _fillTexts() {
    TEXT0[0] = R.string.empty;
    TEXT1[0] = R.string.empty;
    TEXT0[STATE_GREET] = R.string.text1_greet;
    TEXT1[STATE_GREET] = R.string.text2_greet;
    TEXT0[STATE_LOSE] = R.string.text1_lose;
    TEXT1[STATE_LOSE] = R.string.text2_lose;
    TEXT0[STATE_WIN] = R.string.text1_win;
    TEXT1[STATE_WIN] = R.string.text2_win;
    TEXT0[STATE_RUNNING] = R.string.text1_running;
    TEXT1[STATE_RUNNING] = R.string.text2_running;
    TEXT0[STATE_PAUSE] = R.string.text1_pause;
    TEXT1[STATE_PAUSE] = R.string.text2_pause;
    TEXT0[STATE_LOSTLIFE] = R.string.text1_lostlife; 
    TEXT1[STATE_LOSTLIFE] = R.string.text2_lostlife;
    TEXT2[0] = R.string.empty;
    TEXT2[1] = R.string.text_hint1;
    TEXT2[2] = R.string.text_hint2;
    TEXT2[3] = R.string.text_hint3;
    TEXT2[4] = R.string.text_hint4;
    TEXT2[5] = R.string.text_hint5;
    TEXT2[6] = R.string.text_hint6;
    TEXT2[7] = R.string.text_hint7;
    TEXT2[8] = R.string.text_hint8;
  }
  
  public void setTextViews(TextView textView0, TextView textView1, TextView textView2) {
    _text0Text = textView0;
    _text1Text = textView1;
    _text2Text = textView2;
  }

  public void initState() {
    _setSpecificBackground(0);
    _state = STATE_GREET;
    _lastTime = System.currentTimeMillis();
    _initLevel(0);
  }

  public void saveState(Bundle sis) {
    synchronized (_surfaceHolder) {
      if (sis != null) {
        if (_balls != null) {
          Ball[] balls = (Ball[]) _balls.toArray(new Ball[0]);
          sis.putParcelableArray(SIS_BALLS, balls);
        }
        if (_bubbles != null) {
          Bubble[] bubbles = (Bubble[]) _bubbles.toArray(new Bubble[0]);
          sis.putParcelableArray(SIS_BUBBLES, bubbles);
        }
        sis.putInt(SIS_LIVES, _lives);
        sis.putInt(SIS_LEVEL, _level);
        sis.putInt(SIS_BGNUMBER, _bgNumber);
        sis.putInt(SIS_STATE, _state);
      }
    }
  }

  public void restoreState(Bundle sis) {
    synchronized (_surfaceHolder) {
      _state = sis.getInt(SIS_STATE);
      _level = sis.getInt(SIS_LEVEL);
      _lives = sis.getInt(SIS_LIVES);
      _bgNumber = sis.getInt(SIS_BGNUMBER);
      // This cannot be done, because canvas still does not exist. Instead, let _bgBitmap to null, to let _doDraw() set it.
      // _setSpecificBackground(_bgNumber);
      _bgBitmap = null; // Just in case
      Parcelable[] balls = sis.getParcelableArray(SIS_BALLS);
      _balls = new Balls(balls);
      Parcelable[] bubbles = sis.getParcelableArray(SIS_BUBBLES);
      _bubbles = new Bubbles(bubbles);
    }
    _lastTime = System.currentTimeMillis();
    _inflating_r = -1;
  }

  @Override
  public void onWindowFocusChanged(boolean hasWindowFocus) {
    if (hasWindowFocus == false){
      onPause();
    }
  }

  public void onPause() {
    if (_state == STATE_RUNNING) {
      _stopTimer();
      _state = STATE_PAUSE;
      _doLoop();
    }
  }

  private void _fixSize() {
    Canvas canvas = null;
    try {
      canvas = _surfaceHolder.lockCanvas(null);
      _canvasWidth = canvas.getWidth();
      _canvasHeight = canvas.getHeight();
    } finally {
      if (canvas != null) {
        _surfaceHolder.unlockCanvasAndPost(canvas);
      }
    }
    double diagonal = Math.sqrt(_canvasWidth*_canvasWidth+_canvasHeight*_canvasHeight);
    _ball_speed_max = (float)diagonal/BALL_SPEED_SCALE;
    _bubble_deceleration = (float)diagonal/(1000*BUBBLE_DECELERATION_SCALE);
    _bubble_gravity = (float)diagonal/(1000*BUBBLE_GRAVITY_SCALE);
    _bubble_growth = (float)diagonal/(BUBBLE_GROWTH_SCALE);
    _fx_thereshold = (float)diagonal/(FX_THERESHOLD_SCALE);
  }
  
  public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    synchronized (_surfaceHolder) {
      _bgBitmap = Bitmap.createScaledBitmap(_bgBitmap, width, height, true);
    }
    _fixSize();
    _doLoop();
  }

  public void surfaceCreated(SurfaceHolder holder) {
    _fixSize();
    _doLoop();
  }

  public void surfaceDestroyed(SurfaceHolder holder) {
    onPause();
  }

  private void _setRandomHint() {
    int max = 8;
    int r = (int) (Math.random() * max) + 1;
    _setSpecificHint(r);
  }

  // 1-N are "random" hints
  // 0 is empty
  private void _setSpecificHint(int r) {
    _text2Text.setText(_context.getResources().getText(TEXT2[r]));
  }
  
  private void _setRandomBackground() {
    if (Preferences.getUseGallery() && (_context instanceof Activity)) {
      Activity a = (Activity) _context;
      Cursor c = a.managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, null, null, null);
      if (c== null) { // No external card
        Preferences.setUseGallery(false);
        _setRandomBackground();
        return;
      }
      int r = (int) (Math.random() * c.getCount()) + 1;
      c.moveToFirst();
      for (int i=1; i<r; i++) {
        c.moveToNext();
      }
      int image = c.getInt(c.getColumnIndex(MediaStore.Audio.AudioColumns._ID));
      _setSpecificBackground(image);
      c.close();
    } else {
      int r = (int) (Math.random() * MAX_BUILTIN_IMAGES) + 1;
      _setSpecificBackground(r);
    }
  }
  
  // 1-5 are "random" backgrounds
  // 0 is the greeting background
  private void _setSpecificBackground(int r) {
    if (r==0) { // Starting screen background
      _bgBitmap = BitmapFactory.decodeResource(_context.getResources(), R.drawable.background);
      _bgBitmap = Bitmap.createScaledBitmap(_bgBitmap, _canvasWidth, _canvasHeight, true);
      return;
    }
    if ( Preferences.getUseGallery() && (_context instanceof Activity) ) {
      BitmapFactory.Options opts = new BitmapFactory.Options();
      Uri u = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, r);
      try {
        // First find image size, then calculate right zoom, and then actually decode it
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(_context.getContentResolver().openInputStream(u), null, o);
        int scale = 1;
        if (o.outHeight > _canvasHeight || o.outWidth > _canvasWidth) {
          scale = (int) Math.round(Math.pow(2, (long) Math.round(Math.log(_canvasHeight
              / (double) Math.max(o.outHeight, o.outWidth))
              / Math.log(0.5))));
        }
        opts.inSampleSize = scale;
        _bgBitmap = BitmapFactory.decodeStream(_context.getContentResolver().openInputStream(u), null, opts);
        if ( (_canvasWidth > _canvasHeight) && (opts.outWidth < opts.outHeight) ) {
          // Rotate
          Matrix m = new Matrix();
          m.preRotate(-90);
          _bgBitmap= Bitmap.createBitmap(_bgBitmap, 0, 0, _bgBitmap.getWidth(), _bgBitmap.getHeight(), m, true);
        }
        _bgBitmap = Bitmap.createScaledBitmap(_bgBitmap, _canvasWidth, _canvasHeight, true);
        _bgNumber = r;
        return;
      } catch (FileNotFoundException e) {
        // Continue to use a random built-in
        r = (int) (Math.random() * MAX_BUILTIN_IMAGES) + 1;
      }
    }
    switch (r) {
    case 0: // Just in case
      _bgBitmap = BitmapFactory.decodeResource(_context.getResources(), R.drawable.background);
      break;
    case 1:
      _bgBitmap = BitmapFactory.decodeResource(_context.getResources(), R.drawable.bg_01);
      break;
    case 2:
      _bgBitmap = BitmapFactory.decodeResource(_context.getResources(), R.drawable.bg_02);
      break;
    case 3:
      _bgBitmap = BitmapFactory.decodeResource(_context.getResources(), R.drawable.bg_03);
      break;
    case 4:
      _bgBitmap = BitmapFactory.decodeResource(_context.getResources(), R.drawable.bg_04);
      break;
    case 5:
      _bgBitmap = BitmapFactory.decodeResource(_context.getResources(), R.drawable.bg_05);
      break;
    default:
      _setRandomBackground();
      return;
    }
    _bgBitmap = Bitmap.createScaledBitmap(_bgBitmap, _canvasWidth, _canvasHeight, true);
    _bgNumber = r;
  }
  
  private void _resetBounceSounds() {
    _bouncesounds = 0;
  }
  
  private void _playFX(int res, float energy) {
    if (_bouncesounds >= MAX_BOUNCE_SOUND) return;
    if (Preferences.getFxVolume() == 0.0) return;
    // WARNING: this values depend on the mass of the balls and the density of the bubbles
    if (energy<_fx_thereshold) {
      return;
    }
    MediaPlayer m = MediaPlayer.create(_context, res);
    if (m==null) return;
    m.setVolume (Preferences.getFxVolume(), Preferences.getFxVolume());
    m.setOnCompletionListener(new OnCompletionListener() {
      public void onCompletion(MediaPlayer mp) {
        mp.release();
      }
    });
    _bouncesounds++;
    m.start();
  }

  private void _playFX_conditional(MediaPlayer m) {
    if (_bouncesounds >= MAX_BOUNCE_SOUND) return;
    if (Preferences.getFxVolume() == 0.0) return;
    if (m==null) return;
    if (m.isPlaying()) return;
    m.setVolume (Preferences.getFxVolume(), Preferences.getFxVolume());
    _bouncesounds++;
    m.start();
  }

  void _vibrate() {
    if (!Preferences.getVibrate()) return;
    Vibrator v = (Vibrator) _context.getSystemService(Context.VIBRATOR_SERVICE);
    v.vibrate(40);
  }
  
  private void _initLevel(int level) {
    _level = level;
    if (_level == 0) {
      _setSpecificBackground(0);
    } else {
      _setRandomBackground();
    }
    _balls = new Balls(level, _canvasWidth, _canvasHeight, _ball_speed_max);
    _bubbles = new Bubbles();
    _inflating_r = -1;
  }

  private void _initGame() {
    _initLevel(1);
    _lives = _context.getResources().getInteger(R.integer.number_of_lives);
  }
  
  protected void _doDraw() {
    if (_bgBitmap == null) _setSpecificBackground(_bgNumber); // See restoreState();
    _text0Text.setText(_context.getResources().getText(TEXT0[_state]));
    _text1Text.setText(_context.getResources().getText(TEXT1[_state]));
    if ( (_state == STATE_GREET) || (_state == STATE_RUNNING) ) {
      _setSpecificHint(0);
    } else {
      _setRandomHint();
    }
    Canvas canvas = null;
    try {
      canvas = _surfaceHolder.lockCanvas(null);
      if (canvas==null) return; // Just try in next loop
      synchronized (_surfaceHolder) {
        // Background
        canvas.drawColor(0xffc0c0c0);
        canvas.drawBitmap(_bgBitmap, 0, 0, null);
        if (_level == 0) return;
        // Inflating bubble
        if (_inflating_r>0) {
          canvas.drawBitmap(_bubbleBitmap, null, new RectF(_inflating_x, _inflating_y, _inflating_x+2*_inflating_r, _inflating_y+2*_inflating_r), null);
          if (_state == STATE_LOSTLIFE) {
            canvas.drawCircle(_inflating_center_x, _inflating_center_y, _inflating_r, _orangePaint);
          }
        }
        // Bubbles
        for (Bubble b:_bubbles) {
          canvas.drawBitmap(_bubbleBitmap, null, new RectF(b.x, b.y, b.x+2*b.r, b.y+2*b.r), null);
        }
        // Balls
        for (Ball b:_balls) {
          canvas.drawBitmap(_ballBitmap, Math.round(b.x), Math.round(b.y), null);
        }
        // Lives and level
        for (int i = 0; i < _lives; i++) {
          canvas.drawBitmap(_lifeBitmap, i * LIFE_BITMAP_WIDTH, 0, null);
        }
        canvas.drawText(""+_level, 0, LIFE_BITMAP_HEIGTH+LEVEL_FONT_HEIGTH, _orangePaint);
      }
      // Orange dim
      if ( (_state == STATE_PAUSE)
          || (_state == STATE_WIN)
          || (_state == STATE_LOSTLIFE)
          || (_state == STATE_LOSE) ) {
        canvas.drawRect(new Rect(0,0,_canvasWidth, _canvasHeight), _orangePaint);
      }
    } finally {
      if (canvas != null) {
        _surfaceHolder.unlockCanvasAndPost(canvas);
      }
    }
  }

  
  private void _doPhysics() {
    long now = System.currentTimeMillis();
    if (_lastTime > now)
      return;
    long elapsed = now - _lastTime;
    // Log.d("OOO", "Elapsed "+elapsed+"  = now "+now+"  -  LastTime "+_lastTime);
    _lastTime = now;
    _resetBounceSounds();
    _doPhysics_inflate(elapsed);
    _doPhysics_moveBubbles(elapsed);
    _doPhysics_moveBalls(elapsed);
    _doPhysics_bounceBubblesBubbles(elapsed);
    _doPhysics_bounceBubblesBalls(elapsed);
    _doPhysics_bounceBallsBalls(elapsed);
  }

  // Elastic collision: http://en.wikipedia.org/wiki/Elastic_collision
  
  private void _doPhysics_bounceBallsBalls(long elapsed) {
    Object[] balls = _balls.toArray();
    for (int i=0; i<balls.length; i++) {
      for (int j=i+1; j<balls.length; j++) {
        Ball b1 = (Ball)balls[i];
        Ball b2 = (Ball)balls[j];
        float dx = b2.x - b1.x; // (x+r) - (x+r) ==> r is removed
        float dy = b2.y - b1.y; // (y+r) - (y+r) ==> r is removed
        float d = (float)Math.sqrt(dx*dx+dy*dy); 
        if (d > 2*BALL_RAD) continue; // Too far to bounce
        // Vector right and parallel to the bounce
        float rightx = dx;
        float righty = dy;
        float parallelx = righty;
        float parallely = -rightx;
        // Projection of the ball speed on the right and parallel vectors
        float b1_on_right_scale = (b1.vx*rightx+b1.vy*righty) / (rightx*rightx + righty*righty);
        float b1_on_right_x = b1_on_right_scale*rightx;
        float b1_on_right_y = b1_on_right_scale*righty;
        float b1_on_parallel_scale = (b1.vx*parallelx+b1.vy*parallely) / (parallelx*parallelx + parallely*parallely);
        float b1_on_parallel_x = b1_on_parallel_scale*parallelx;
        float b1_on_parallel_y = b1_on_parallel_scale*parallely;
        // Projection of the ball speed on the right and parallel vectors
        float b2_on_right_scale = (b2.vx*rightx+b2.vy*righty) / (rightx*rightx + righty*righty);
        float b2_on_right_x = b2_on_right_scale*rightx;
        float b2_on_right_y = b2_on_right_scale*righty;
        float b2_on_parallel_scale = (b2.vx*parallelx+b2.vy*parallely) / (parallelx*parallelx + parallely*parallely);
        float b2_on_parallel_x = b2_on_parallel_scale*parallelx;
        float b2_on_parallel_y = b2_on_parallel_scale*parallely;
        // Calculate the right components
        float b1_on_right_x_new = b2_on_right_x;
        float b1_on_right_y_new = b2_on_right_y;
        float b2_on_right_x_new = b1_on_right_x;
        float b2_on_right_y_new = b1_on_right_y;
        // Compose the speeds again add the right and parallel components
        b1.vx = b1_on_right_x_new + b1_on_parallel_x;
        b1.vy = b1_on_right_y_new + b1_on_parallel_y;
        b2.vx = b2_on_right_x_new + b2_on_parallel_x;
        b2.vy = b2_on_right_y_new + b2_on_parallel_y;
        // Move the balls apart until they do not overlap
        // First, check whether one ball went beyond the other
        float deltav_x = b1_on_right_x - b2_on_right_x;
        float deltav_y = b1_on_right_y - b2_on_right_y;
        float escalar = deltav_x*dx + deltav_y*dy;
        float k = 0;
        if (escalar < 0) {
          // One ball passed beyond the other
//          int R = Math.max(BALL_RAD, BALL_RAD);
//          int r = Math.min(BALL_RAD, BALL_RAD);
//          float k = (2*R + 2*r - 2*BALL_RAD + d)/2;
//          float k = (R + r - BALL_RAD + d/2);
//          float k = (r1 + r2 - BALL_RAD + d/2);
          k = -(2*BALL_RAD + d)/2;
        } else {
          k = (2*BALL_RAD - d)/2;
        }
        double alpha = Math.atan(dy / dx);
        if (dx < 0) alpha += Math.PI;
        b2.x += k * Math.cos(alpha);
        b2.y += k * Math.sin(alpha);
        b1.x -= k * Math.cos(alpha);
        b1.y -= k * Math.sin(alpha);
        // Rough estimation of energy involved in the bounce, as thereshold for fx
        float energy_x = b1_on_right_x - b2_on_right_x;
        float energy_y = b1_on_right_y - b2_on_right_y;
        float energy = (energy_x*energy_x + energy_y*energy_y)*(b1.MASS+b2.MASS)/2;
        if (energy > _fx_thereshold) {
          _playFX(R.raw.bounce_ballball, energy);
        }
        _vibrate();
      }
    }
  }
  
  private void _doPhysics_moveBalls(long elapsed) {
    for (Ball b:_balls) {
      b.x += (b.vx*elapsed);
      b.y += (b.vy*elapsed);
      if (Math.round(b.x) < 0) {
        b.x = 0;
        b.vx = Math.abs(b.vx);
        _playFX(R.raw.bounce_wallball, b.MASS*b.vx*b.vx);
        _vibrate();
      }
      if (Math.round(b.x) > (_canvasWidth-2*BALL_RAD) ) {
        b.x = (_canvasWidth-2*BALL_RAD);
        b.vx = -Math.abs(b.vx);
        _playFX(R.raw.bounce_wallball, b.MASS*b.vx*b.vx);
        _vibrate();
      }
      if (Math.round(b.y) < 0) {
        b.y = 0;
        b.vy = Math.abs(b.vy);
        _playFX(R.raw.bounce_wallball, b.MASS*b.vy*b.vy);
        _vibrate();
      }
      if (Math.round(b.y) > (_canvasHeight-2*BALL_RAD) ) {
        b.y = (_canvasHeight-2*BALL_RAD);
        b.vy = -Math.abs(b.vy);
        _playFX(R.raw.bounce_wallball, b.MASS*b.vy*b.vy);
        _vibrate();
      }
    }
  }

  private void _doPhysics_bounceBubblesBalls(long elapsed) {
    Object[] bubbles = _bubbles.toArray();
    Object[] balls = _balls.toArray();
    for (int i=0; i<bubbles.length; i++) {
      for (int j=0; j<balls.length; j++) {
        Bubble b1 = (Bubble)bubbles[i];
        Ball b2 = (Ball)balls[j];
        float dx = (b2.x+BALL_RAD) - (b1.x+b1.r);
        float dy = (b2.y+BALL_RAD) - (b1.y+b1.r);
        float d = (float)Math.sqrt(dx*dx+dy*dy); 
        if ( d > (b1.r+BALL_RAD)) continue; // Too far to bounce
        // Vector right and parallel to the bounce
        float rightx = dx;
        float righty = dy;
        float parallelx = righty;
        float parallely = -rightx;
        // Projection of the bubble speed on the right and parallel vectors
        float b1_on_right_scale = (b1.vx*rightx+b1.vy*righty) / (rightx*rightx + righty*righty);
        float b1_on_right_x = b1_on_right_scale*rightx;
        float b1_on_right_y = b1_on_right_scale*righty;
        float b1_on_parallel_scale = (b1.vx*parallelx+b1.vy*parallely) / (parallelx*parallelx + parallely*parallely);
        float b1_on_parallel_x = b1_on_parallel_scale*parallelx;
        float b1_on_parallel_y = b1_on_parallel_scale*parallely;
        // Projection of the bubble speed on the right and parallel vectors
        float b2_on_right_scale = (b2.vx*rightx+b2.vy*righty) / (rightx*rightx + righty*righty);
        float b2_on_right_x = b2_on_right_scale*rightx;
        float b2_on_right_y = b2_on_right_scale*righty;
        float b2_on_parallel_scale = (b2.vx*parallelx+b2.vy*parallely) / (parallelx*parallelx + parallely*parallely);
        float b2_on_parallel_x = b2_on_parallel_scale*parallelx;
        float b2_on_parallel_y = b2_on_parallel_scale*parallely;
        // Calculate the right components
        float b1_on_right_x_new = (b1_on_right_x*(b1.m-b2.MASS)+2*b2.MASS*b2_on_right_x) / (b1.m+b2.MASS);
        float b1_on_right_y_new = (b1_on_right_y*(b1.m-b2.MASS)+2*b2.MASS*b2_on_right_y) / (b1.m+b2.MASS);
        float b2_on_right_x_new = (b2_on_right_x*(b2.MASS-b1.m)+2*b1.m*b1_on_right_x) / (b2.MASS+b1.m);
        float b2_on_right_y_new = (b2_on_right_y*(b2.MASS-b1.m)+2*b1.m*b1_on_right_y) / (b2.MASS+b1.m);
        // Measure ball speed, for it must be kept
        float b2_speed_old = (float)Math.sqrt(b2.vx*b2.vx+b2.vy*b2.vy); 
        // Compose the speeds again add the right and parallel components
        b1.vx = b1_on_right_x_new + b1_on_parallel_x;
        b1.vy = b1_on_right_y_new + b1_on_parallel_y;
        b2.vx = b2_on_right_x_new + b2_on_parallel_x;
        b2.vy = b2_on_right_y_new + b2_on_parallel_y;
        float b2_speed_new = (float)Math.sqrt(b2.vx*b2.vx+b2.vy*b2.vy);
        float b2_speed_scale = b2_speed_old / b2_speed_new;
        b2.vx *= b2_speed_scale;
        b2.vy *= b2_speed_scale;
        // Move the balls apart until they do not overlap
        // First, check whether one ball went beyond the other
        float deltav_x = b1_on_right_x - b2_on_right_x;
        float deltav_y = b1_on_right_y - b2_on_right_y;
        float escalar = deltav_x*dx + deltav_y*dy;
        float k = 0;
        if (escalar < 0) {
          // One ball passed beyond the other
          k = -(b1.r + BALL_RAD + d)/2;
        } else {
          k = (b1.r + BALL_RAD - d)/2;
        }
        double alpha = Math.atan(dy/dx);
        if (dx<0) alpha += Math.PI;
        b2.x += k*Math.cos(alpha);
        b2.y += k*Math.sin(alpha);
        b1.x -= k*Math.cos(alpha);
        b1.y -= k*Math.sin(alpha);
        // Rough estimation of energy involved in the bounce, as thereshold for fx
        float energy_x = b1_on_right_x - b2_on_right_x;
        float energy_y = b1_on_right_y - b2_on_right_y;
        float energy = (energy_x*energy_x + energy_y*energy_y)*(b1.m+b2.MASS)/2;
        if (energy > _fx_thereshold) {
          _playFX(R.raw.bounce_ballbubble, energy);
        }
        _vibrate();
      }
    }
  }

  private void _doPhysics_bounceBubblesBubbles(long elapsed) {
    Object[] bubbles = _bubbles.toArray();
    for (int i=0; i<bubbles.length; i++) {
      for (int j=i+1; j<bubbles.length; j++) {
        Bubble b1 = (Bubble)bubbles[i];
        Bubble b2 = (Bubble)bubbles[j];
        float dx = (b2.x+b2.r) - (b1.x+b1.r);
        float dy = (b2.y+b2.r) - (b1.y+b1.r);
        float d = (float)Math.sqrt(dx*dx+dy*dy); 
        if ( d > (b1.r+b2.r)) continue; // Too far to bounce
        // Vector right and parallel to the bounce
        float rightx = dx;
        float righty = dy;
        float parallelx = righty;
        float parallely = -rightx;
        // Projection of the bubble speed on the right and parallel vectors
        float b1_on_right_scale = (b1.vx*rightx+b1.vy*righty) / (rightx*rightx + righty*righty);
        float b1_on_right_x = b1_on_right_scale*rightx;
        float b1_on_right_y = b1_on_right_scale*righty;
        float b1_on_parallel_scale = (b1.vx*parallelx+b1.vy*parallely) / (parallelx*parallelx + parallely*parallely);
        float b1_on_parallel_x = b1_on_parallel_scale*parallelx;
        float b1_on_parallel_y = b1_on_parallel_scale*parallely;
        // Projection of the bubble speed on the right and parallel vectors
        float b2_on_right_scale = (b2.vx*rightx+b2.vy*righty) / (rightx*rightx + righty*righty);
        float b2_on_right_x = b2_on_right_scale*rightx;
        float b2_on_right_y = b2_on_right_scale*righty;
        float b2_on_parallel_scale = (b2.vx*parallelx+b2.vy*parallely) / (parallelx*parallelx + parallely*parallely);
        float b2_on_parallel_x = b2_on_parallel_scale*parallelx;
        float b2_on_parallel_y = b2_on_parallel_scale*parallely;
        // Calculate the right components
        float b1_on_right_x_new = (b1_on_right_x*(b1.m-b2.m)+2*b2.m*b2_on_right_x) / (b1.m+b2.m);
        float b1_on_right_y_new = (b1_on_right_y*(b1.m-b2.m)+2*b2.m*b2_on_right_y) / (b1.m+b2.m);
        float b2_on_right_x_new = (b2_on_right_x*(b2.m-b1.m)+2*b1.m*b1_on_right_x) / (b2.m+b1.m);
        float b2_on_right_y_new = (b2_on_right_y*(b2.m-b1.m)+2*b1.m*b1_on_right_y) / (b2.m+b1.m);
        // Compose the speeds again add the right and parallel components
        b1.vx = b1_on_right_x_new + b1_on_parallel_x;
        b1.vy = b1_on_right_y_new + b1_on_parallel_y;
        b2.vx = b2_on_right_x_new + b2_on_parallel_x;
        b2.vy = b2_on_right_y_new + b2_on_parallel_y;
        // Move the balls apart until they do not overlap
        // First, check whether one ball went beyond the other
        // Do not do the check; since they move slowly, this may cause false positives when bouncing in walls
        float k = (b1.r + b2.r - d)/2;
//        float deltav_x = b1_on_right_x - b2_on_right_x;
//        float deltav_y = b1_on_right_y - b2_on_right_y;
//        float escalar = deltav_x*dx + deltav_y*dy;
//        float k = 0;
//        if (escalar < 0) {
//          // One ball passed beyond the other
//          k = -(b1.r + b2.r + d)/2;
//        } else {
//          k = (b1.r + b2.r - d)/2;
//        }
        double alpha = Math.atan(dy/dx);
        if (dx<0) alpha += Math.PI;
        b2.x += k*Math.cos(alpha);
        b2.y += k*Math.sin(alpha);
        b1.x -= k*Math.cos(alpha);
        b1.y -= k*Math.sin(alpha);
        // Rough estimation of energy involved in the bounce, as thereshold for fx
        float energy_x = b1_on_right_x - b2_on_right_x;
        float energy_y = b1_on_right_y - b2_on_right_y;
        float energy = (energy_x*energy_x + energy_y*energy_y)*(b1.m+b2.m)/2;
        if (energy > _fx_thereshold) {
          _playFX(R.raw.bounce_bubblebubble, energy);
        }
        _vibrate();
      }
    }
  }

  private void _doPhysics_moveBubbles(long elapsed) {
    for (Bubble b:_bubbles) {
      // Decelerate by friction.
      // The right deceleration is o(r^2), but that's too much difference between
      // large and small bubbles for a funny game
      if (b.vx>0) {
        b.vx -= _bubble_deceleration*elapsed;
        if (b.vx<0) b.vx=0;
      } else {
        b.vx += _bubble_deceleration*elapsed;
        if (b.vx>0) b.vx=0;
      }
      if (b.vy>0) {
        b.vy -= _bubble_deceleration*elapsed;
        if (b.vy<0) b.vy=0;
      } else {
        b.vy += _bubble_deceleration*elapsed;
        if (b.vy>0) b.vy=0;
      }
      // Accelerate by gravity. Optimization: do not accelerate if resting on the floor
      if ( (b.y+b.r+b.r+1) < (_canvasHeight) ) {
        b.vy += (_bubble_gravity*elapsed);
      }
      // Move
      b.x += (b.vx*elapsed);
      b.y += (b.vy*elapsed);
      // Bounce in walls
      if (Math.round(b.x) < 0) {
        b.x = 0;
        b.vx = Math.abs(b.vx);
        _playFX(R.raw.bounce_wallbubble, b.m*b.vx*b.vx);
        _vibrate();
      }
      if (Math.round(b.x) > (_canvasWidth-2*b.r) ) {
        b.x = (_canvasWidth-2*b.r);
        b.vx = -Math.abs(b.vx);
        _playFX(R.raw.bounce_wallbubble, b.m*b.vx*b.vx);
        _vibrate();
      }
      if (Math.round(b.y) < 0) {
        b.y = 0;
        b.vy = Math.abs(b.vy);
        _playFX(R.raw.bounce_wallbubble, b.m*b.vy*b.vy);
        _vibrate();
      }
      if (Math.round(b.y) > (_canvasHeight-2*b.r) ) {
        b.y = (_canvasHeight-2*b.r);
        b.vy = -Math.abs(b.vy);
        _playFX(R.raw.bounce_wallbubble, b.m*b.vy*b.vy);
        _vibrate();
      }
    }
  }

  private void _doPhysics_inflate(long elapsed) {
    if (_inflating_r<0) return;
    _inflating_r += _bubble_growth*elapsed;
    _inflating_x = _inflating_center_x - _inflating_r;
    _inflating_y = _inflating_center_y - _inflating_r;
    // Check death :-(
    if (_inflating_x < 0) _doPhysics_inflate_lostlife();
    if ((_inflating_x+2*_inflating_r) > _canvasWidth) _doPhysics_inflate_lostlife();
    if (_inflating_y < 0) _doPhysics_inflate_lostlife();
    if ((_inflating_y+2*_inflating_r) > _canvasHeight) _doPhysics_inflate_lostlife();
    for (Ball b:_balls) {
      float dx = (b.x+BALL_RAD) - (_inflating_x+_inflating_r);
      float dy = (b.y+BALL_RAD) - (_inflating_y+_inflating_r);
      float d = (float)Math.sqrt(dx*dx+dy*dy); 
      if ( d < (_inflating_r+BALL_RAD)) _doPhysics_inflate_lostlife();
    }
    for (Bubble b:_bubbles) {
      float dx = (b.x+b.r) - (_inflating_x+_inflating_r);
      float dy = (b.y+b.r) - (_inflating_y+_inflating_r);
      float d = (float)Math.sqrt(dx*dx+dy*dy); 
      if ( d < (_inflating_r+b.r)) _doPhysics_inflate_lostlife();
    }
  }
  
  private void _doPhysics_inflate_lostlife() {
    _state = STATE_LOSTLIFE;
    _lives--;
    _playFX_conditional(_bangMedia);
  }
  
  private void _doLoop() {
    switch (_state) {
    case (STATE_GREET):
      _stopTimer(); // Just in case
      _initLevel(0);
      _inflating_r = -1;
      _doDraw();
      break;
    case (STATE_PAUSE):
      _stopTimer(); // Just in case
      _inflating_r = -1;
      _doDraw();
      break;
    case (STATE_RUNNING):
      _doDraw();
      _restartTimer();
      break;
    case (STATE_WIN):
      _stopTimer(); // Just in case
      _doDraw();
      _inflating_r = -1;
      break;
    case (STATE_LOSE):
      _stopTimer(); // Just in case
      _doDraw();
      _inflating_r = -1;
      break;
    case (STATE_LOSTLIFE):
      _stopTimer(); // Just in case
      _doDraw();
      _inflating_r = -1;
      break;
    default:
      Log.e(this.getClass().getName(), "Should not reach default in switch");
      break;
    }
  }

  public void onClick(View v) {
    switch (_state) {
    case (STATE_GREET):
      _initGame();
      _lastTime = _lastSleep = System.currentTimeMillis();
      _state = STATE_RUNNING;
      break;
    case (STATE_PAUSE):
      _lastTime = _lastSleep = System.currentTimeMillis();
      _state = STATE_RUNNING;
      break;
    case (STATE_RUNNING):
      // Do nothing
      break;
    case (STATE_WIN):
      _initLevel(_level+1);
      _lastTime = _lastSleep = System.currentTimeMillis();
      _state = STATE_RUNNING;
      break;
    case (STATE_LOSE):
      _state = STATE_GREET;
      break;
    case (STATE_LOSTLIFE):
      if (_lives > 0) {
        _lastTime = _lastSleep = System.currentTimeMillis();
        _state = STATE_RUNNING;
      } else {
        _state = STATE_LOSE;
      }
      break;
    default:
      Log.e(this.getClass().getName(), "Should not reach default in switch");
      _state = STATE_GREET;
      break;
    }
    _doLoop();
  }
  
  public boolean onTouch(View v, MotionEvent e){
    boolean res = false;
    switch (_state) {
    case (STATE_GREET):
      break;
    case (STATE_PAUSE):
      break;
    case (STATE_RUNNING):
      res = _onTouch_running(e);
      break;
    case (STATE_WIN):
      break;
    case (STATE_LOSE):
      break;
    case (STATE_LOSTLIFE):
      break;
    default:
      Log.e(this.getClass().getName(), "Should not reach default in switch");
      _state = STATE_GREET;
      break;
    }
    return res;
  }

  private boolean _onTouch_running(MotionEvent e) {
    boolean res = false;
    switch (e.getAction()) {
    case MotionEvent.ACTION_DOWN:
      _inflating_r = 0;
      _inflating_center_x = Math.round(e.getX());
      _inflating_center_y = Math.round(e.getY());
      res = true;
    break;
    case MotionEvent.ACTION_MOVE:
      //_inflating_center_x = Math.round(e.getX());
      //_inflating_center_y = Math.round(e.getY());
      res = true;
    break;
    case MotionEvent.ACTION_UP:
      if (_inflating_r > 0) {
        while (_bubbles.size() > MAX_BUBBLES) {
          // int r = (int) (Math.random() * _bubbles.size());
          _bubbles.remove(0);
        }
        _bubbles.add(new Bubble(_inflating_x, _inflating_y, 0f, 0f, _inflating_r));
      }
      _inflating_r = -1;
      float area = 0;
      for (Bubble b:_bubbles) {
        area += b.area;
      }
      if ( (area/(_canvasWidth*_canvasHeight)) > AREA_TARGET ) {
        _stopTimer();
        _playFX_conditional(_applauseMedia);
        _state = STATE_WIN;
        _doLoop();
      }
      res = true;
    break;
    default:
    break;
    }
    return res;
  }
  
  public void onTimer() {
    _lastSleep = System.currentTimeMillis();
    _doPhysics(); // This may change _state to LOSTLIFE, so do not assume RUNNING
    _doLoop();
  }
  private void _startTimer() {
    long now = System.currentTimeMillis();
    long sleep = MS_PER_FRAME - now + _lastSleep;
    // Log.d("111", "Wasted "+(now-_lastSleep)+" --> Sleep "+sleep);
    if (sleep > (MS_PER_FRAME/3)) { // Magic. If we are about to sleep to few time, just don't sleep
      timer.postDelayed(onTimerRunnable, sleep);
    } else {
      timer.post(onTimerRunnable);
    }
  }
  private void _stopTimer() {
    timer.removeCallbacks(onTimerRunnable);
  }
  private void _restartTimer() {
    _stopTimer();
    _startTimer();
  }
  
}
